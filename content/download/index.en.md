---
title: "Download"
date: 2018-10-13T22:07:56+02:00
draft: false
type: "page"
menu:
  page:
    title: "Download"
    weight: 30
---

# Downloads

If you find any bugs or other issues, please [report them](https://code.vikunja.io/api/issues).

Please also check out [the docs](https://vikunja.io/docs/installing/) for information about how to install Vikunja on your own server.

Every part of Vikunja is available as an unstable or release build.
Unstable builds contain the latest changes and features, but are experimental or may contain bugs.
[Check out this docs article](https://vikunja.io/docs/versions/) to learn more.

## API-Server

If you want to host Vikunja, you have to install this.

You can get all api releases from [our download server](https://dl.vikunja.io/api/).

You can also [get the source code](https://code.vikunja.io/api) and compile it yourself. 
Instructions on how to do this can be found [here](https://vikunja.io/docs/build-from-sources/#api).

The api is available as `.deb`, `.apk` (alpine) and `.rpm` package.
Just look for the files in the respective release folder.

[A docker image](https://vikunja.io/docs/full-docker-example/) is available as well.

## Frontend

You can get all frontend releases from [our download server](https://dl.vikunja.io/frontend/).

You can also [get the source code](https://code.vikunja.io/frontend) and build it yourself. Instructions on how to do this can be found [here](https://vikunja.io/docs/build-from-sources/#frontend).

[A docker image](https://vikunja.io/docs/full-docker-example/) is available as well.

## Desktop apps

The frontend is available as standalone desktop app.
This allows to use Vikunja without installing a frontend.

To install it, simply go to [our download server](https://dl.vikunja.io/desktop) choose the latest version and the right variant for your platform.

## Mobile apps

The app is currently in the early stages of development. Even though it can be used, it might not be stable. If you have bug reports or feature requests, please post them on [this](https://github.com/go-vikunja/app/issues) GitHub page.
Android users can download a prebuilt apk from the [releases on GitHub](https://github.com/go-vikunja/app/releases/latest).
We'll publish it to F-Droid and the Google Play Store once the app is feature complete.
There are no resources to actively support iOS right now. However, you might be able to get it to work with the source code from [GitHub](https://github.com/go-vikunja/app). 
We're also always happy to accept pull requests.

## CLI app

There is a third-party cli app, written in Python.
Please check out [the project's repository](https://gitlab.com/ce72/vja) for information on how to install and use it.

